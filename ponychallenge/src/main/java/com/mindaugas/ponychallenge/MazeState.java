package com.mindaugas.ponychallenge;

import com.google.gson.annotations.SerializedName;

public  class MazeState {
	int[] pony;
	int[] domokun;
	@SerializedName("end-point") int[] endPoint;
	int[] size;
	int difficulty;
	String[][] data;
	@SerializedName("maze_id") String mazeId;
	@SerializedName("game-state") GameState gameState;
}
