package com.mindaugas.ponychallenge;

/**
 * 
 * @author mindaugas
 *
 */
public enum Directions {
	NORTH(2, "north"),
	SOUTH(4, "south"),
	EAST(8, "east"),
	WEST(16, "west");
	
	private final int direction;
	public int getDirection() {
		return direction;
	}
	private final String name;
	public String getName() {
		return name;
	}
	private Directions(final int direction, final String name) {
		this.direction = direction;
		this.name = name;
	}
}
