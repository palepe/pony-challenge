package com.mindaugas.ponychallenge;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * 
 * @author mindaugas
 *
 */
public class MazeGame {

	private static final Node NOT_VISITED = null;
	private static final int NO_PARENT = -1;
	private final static Logger logger = Logger.getLogger(MazeGame.class);
	
	private int mazeWidth, mazeHeight, endPoint;
	private String mazeId;
	private int pony, domokun;
	private GameState state;
	private final MazeGameHelper mazeGameHelper;
	private Maze maze;
	private Set<Integer> visited = new HashSet<>();
	
	public MazeGame(final MazeGameHelper mazeGameHelper) {
		this.mazeGameHelper = mazeGameHelper;
	}
	
	public GameState start(final int mazeWidth, final int mazeHeight, final String playerName, final int difficulty) {
		this.mazeWidth = mazeWidth;
		this.mazeHeight = mazeHeight;
		
		this.mazeId = mazeGameHelper.createNewGame(mazeWidth, mazeHeight, playerName, difficulty);
		logger.debug("Maze ID: " + mazeId);
		final MazeState mazeState = mazeGameHelper.getMazeCurrentState(mazeId);
		this.endPoint = mazeState.endPoint[0];
		this.pony = mazeState.pony[0];
		this.domokun = mazeState.domokun[0];
		this.state = mazeState.gameState;
		
		this.maze = new Maze(mazeWidth, mazeHeight, endPoint, mazeState.data);
		
		return this.state;
	}
	
	public GameState makeNextMove() {
		final Directions nextMove = proposeNextMove();
		state = mazeGameHelper.makeMove(mazeId, nextMove.getName());
		
		final MazeState mazeState = mazeGameHelper.getMazeCurrentState(mazeId);
		visited.add(pony);
		visited.add(domokun);
		
		pony = mazeState.pony[0];
		domokun = mazeState.domokun[0];
		
		return state;
	}
	
	private Directions proposeNextMove() {
		final Path ponyPath = findPathToEndpoint(false);
		final Node nextPonyMove = nextMove(ponyPath.lastVisited.id, ponyPath.nodes);
		final Path domokunPathToNextponyMove = findPath(domokun, nextPonyMove.id);
		final Node nextDomokunMove = nextMove(nextPonyMove.id, domokunPathToNextponyMove.nodes);
		if (nextPonyMove.id == domokun || nextDomokunMove.id == nextPonyMove.id) {
			final Path pathToAvoidDomokun = findPathToEndpoint(true);
			if (pathToAvoidDomokun.lastVisited == null) {
				// the monster will eat now .. :(
				if (maze.canGoNorth(pony)) {
					return Directions.NORTH;
				}
				if (maze.canGoSouth(pony)) {
					return Directions.SOUTH;
				}
				if (maze.canGoEast(pony)) {
					return Directions.EAST;
				}
				if (maze.canGoWest(pony)) {
					return Directions.WEST;
				}
			}
			final Node nextPonyMoveToAvoidDomokun = nextMove(pathToAvoidDomokun.lastVisited.id, pathToAvoidDomokun.nodes);
			return nextPonyMoveToAvoidDomokun.direction;
		}
		return nextPonyMove.direction;
	}

	private static Node nextMove(final int end, final Node[] ponyPath) {
		Node nextPonyMove = new Node(-1, end, null);
		while (ponyPath[nextPonyMove.parentId].parentId != NO_PARENT) {
			nextPonyMove = ponyPath[nextPonyMove.parentId];
		}
		return nextPonyMove;
	}
	
	private Path findPathToEndpoint(final boolean avoidDomokun) {
		final Node[] visited = new Node[mazeWidth * mazeHeight];
		if (avoidDomokun) {
			visited[domokun] = new Node(domokun, NO_PARENT, null);
			if (maze.canGoNorth(domokun)) {
				visited[domokun - mazeWidth] = new Node(domokun - mazeWidth, NO_PARENT, null);
			}
			if (maze.canGoSouth(domokun)) {
				visited[domokun + mazeWidth] = new Node(domokun + mazeWidth, NO_PARENT, null);
			}
			if (maze.canGoEast(domokun)) {
				visited[domokun + 1] = new Node(domokun + 1, NO_PARENT, null);
			}
			if (maze.canGoWest(domokun)) {
				visited[domokun - 1] = new Node(domokun - 1, NO_PARENT, null);
			}
		}
		return findPath(pony, endPoint, mazeWidth, maze, visited);
	}
	
	private Path findPath(final int start, final int end) {
		final Node[] visited = new Node[mazeWidth * mazeHeight];
		return findPath(start, end, mazeWidth, maze, visited);
	}
	
	private static Path findPath(final int start, final int end, final int mazeWidth, final Maze maze, final Node[] visited) {

		final Queue<Integer> queue = new LinkedList<>();
		queue.add(start);
		
		visited[start] = new Node(start, NO_PARENT, null);
		
		Node lastVisited = null;
		while (!queue.isEmpty()) {
			final int current = queue.remove();
			
			final int up = current - mazeWidth;
			if (maze.canGoNorth(current) && visited[up] == NOT_VISITED) {
				lastVisited = new Node(up, current, Directions.NORTH);
				visited[up] = lastVisited;
				if (up == end) {
					return new Path(visited, lastVisited);
				}
				queue.add(up);
			}
			final int down = current + mazeWidth;
			if (maze.canGoSouth(current) && visited[down] == NOT_VISITED) {
				lastVisited = new Node(down, current, Directions.SOUTH);
				visited[down] = lastVisited;
				if (down == end) {
					return new Path(visited, lastVisited);
				}
				queue.add(down);
			}
			final int right = current + 1;
			if (maze.canGoEast(current) && visited[right] == NOT_VISITED) {
				lastVisited = new Node(right, current, Directions.EAST);
				visited[right] = lastVisited;
				if (right == end) {
					return new Path(visited, lastVisited);
				}
				queue.add(right);
			}
			final int left = current - 1;
			if (maze.canGoWest(current) && visited[left] == NOT_VISITED) {
				lastVisited = new Node(left, current, Directions.WEST);
				visited[left] = lastVisited;
				if (left == end) {
					return new Path(visited, lastVisited);
				}
				queue.add(left);
			}
		}
		
		return new Path(visited, lastVisited);
	}
	
	private static class Path {
		private final Node[] nodes;
		private final Node lastVisited;
		Path(final Node[] nodes, final Node lastVisited) {
			this.nodes = nodes;
			this.lastVisited = lastVisited;
		}
	}
	
	private static class Node {
		private final int id;
		private final int parentId;
		private final Directions direction;
		
		Node(final int id, final int parentId, final Directions direction) {
			this.id = id;
			this.parentId = parentId;
			this.direction = direction;
		}
	}

	public void printMaze() {
		this.maze.print(pony, domokun, visited);
	}
}
