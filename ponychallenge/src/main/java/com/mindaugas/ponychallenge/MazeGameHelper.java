package com.mindaugas.ponychallenge;

public interface MazeGameHelper {

	String createNewGame(int mazeWidth, int mazeHeight, String playerName, int difficulty);

	MazeState getMazeCurrentState(String mazeId);

	GameState makeMove(String mazeId, String direction);

}