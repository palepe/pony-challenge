package com.mindaugas.ponychallenge.trustpilot;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.JsonObject;

/**
 * 
 * @author mindaugas 
 * Inspired from:
 * https://stackoverflow.com/questions/7181534/http-post-using-json-in-java
 */
public class JSONClient {

	public String executePOSTRequest(final String url, final JsonObject inputParams) throws ClientProtocolException, IOException {
		
		try (final CloseableHttpClient httpClient = HttpClientBuilder.create().build();) {
			final HttpPost request = new HttpPost(url);
			final StringEntity params = new StringEntity(inputParams.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);

			try (final CloseableHttpResponse response = httpClient.execute(request);
					final InputStream inputStream = response.getEntity().getContent();) {
				return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
			}
		}
	}

	public String executeGETRequest(final String url) throws ClientProtocolException, IOException {
		
		try (final CloseableHttpClient httpClient = HttpClientBuilder.create().build();) {
			final HttpGet request = new HttpGet(url);
			request.addHeader("content-type", "application/json");

			try (final CloseableHttpResponse response = httpClient.execute(request);
					final InputStream inputStream = response.getEntity().getContent();) {
				return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
			}
		}
	}

}
