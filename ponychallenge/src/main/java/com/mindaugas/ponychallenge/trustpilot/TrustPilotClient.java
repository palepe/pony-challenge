package com.mindaugas.ponychallenge.trustpilot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.mindaugas.ponychallenge.GameState;
import com.mindaugas.ponychallenge.MazeGameHelper;
import com.mindaugas.ponychallenge.MazeState;

/**
 * 
 * @author mindaugas
 *
 */
public class TrustPilotClient implements MazeGameHelper {

	private final String hostURL = "https://ponychallenge.trustpilot.com/pony-challenge/";
	
	private final JSONClient jsonClient = new JSONClient();
	
	private final Gson gson = new GsonBuilder().create();
	
	@Override
	public String createNewGame(final int mazeWidth, final int mazeHeight, final String playerName, final int difficulty) {
		final JsonObject inputParams = new JsonObject();
		inputParams.addProperty("maze-width", mazeWidth);
		inputParams.addProperty("maze-height", mazeHeight);
		inputParams.addProperty("maze-player-name", playerName);
		inputParams.addProperty("difficulty", difficulty);
		String response = "";
		try {
			response = jsonClient.executePOSTRequest(hostURL + "maze", inputParams);
			final MazeIDInternal mazeId = gson.fromJson(response, MazeIDInternal.class);
			return mazeId.mazeId;
		} catch (JsonSyntaxException e) {
			throw new IllegalArgumentException(response);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public MazeState getMazeCurrentState(final String mazeId) {
		try {
			final String response = jsonClient.executeGETRequest(hostURL + "maze/" + mazeId);
			return gson.fromJson(response, MazeState.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public GameState makeMove(final String mazeId, final String direction) {
		final JsonObject inputParams = new JsonObject();
		inputParams.addProperty("direction", direction);
		try {
			final String response = jsonClient.executePOSTRequest(hostURL + "maze/" + mazeId, inputParams);
			return gson.fromJson(response, GameState.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static class MazeIDInternal {
		@SerializedName("maze_id") private String mazeId;
	}
}
