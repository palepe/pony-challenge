package com.mindaugas.ponychallenge;

import com.mindaugas.ponychallenge.trustpilot.TrustPilotClient;

/**
 * 
 * @author mindaugas
 *
 */
public class App {
	
	public static void main(String[] args) {
		try {
			final MazeGame mazeGame = new MazeGame(new TrustPilotClient());
			GameState gameState = mazeGame.start(15, 15, "pinkie pie", 5);
			while (gameState.state == State.ACTIVE) {
				gameState = mazeGame.makeNextMove();
				mazeGame.printMaze();
			}
			System.out.println(gameState.stateResult);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
