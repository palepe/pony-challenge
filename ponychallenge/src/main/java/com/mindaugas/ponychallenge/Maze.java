package com.mindaugas.ponychallenge;

import java.util.Set;

public class Maze {
	
	private final int width;
	private final int height;
	private final int endPoint;
	private final int size;
	private byte[] maze;
	
	public Maze(final int width, final int height, final int endPoint, final String[][] walls) {
		this.width = width;
		this.height = height;
		this.endPoint = endPoint;
		this.size = width * height;
		this.maze = new byte[size];
		
		for (int i = 0; i < size; i++) {
			for (final String wall : walls[i]) {
				this.maze[i] = (byte)(convert(wall).getDirection() | this.maze[i]);				
			}
		}
	}
	
	private Directions convert(final String str) {
		switch (str) {
		case "north":
			return Directions.NORTH;
		case "west":
			return Directions.WEST;
		}
		throw new IllegalArgumentException(str);
	}
	
	public void print(final int pony, final int domokun, final Set<Integer> visited) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (canGoNorth(i * width + j)) {
					System.out.print("+   ");
				} else {
					System.out.print("+---");
				}
			}
			System.out.println("+");
			for (int j = 0; j < width; j++) {
				System.out.print(representPosition(i * width + j, pony, domokun, endPoint, visited));
			}
			System.out.println("|");
		}
		for (int i = 0; i < width; i++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}

	private String representPosition (final int currentPosition, final int pony, final int domokun, final int endPoint, final Set<Integer> visited) {
		final StringBuilder builder = new StringBuilder();
		if (canGoWest(currentPosition)) {
			builder.append("  ");
		} else {
			builder.append("| ");
		}
		if (currentPosition == domokun) {
			return builder.append("D ").toString();
		}
		if (currentPosition == pony) {
			return builder.append("P ").toString();
		}
		if (currentPosition == endPoint) {
			return builder.append("E ").toString();
		}
		for (final int vBar : visited) {
			if (currentPosition == vBar) {
				return builder.append("* ").toString(); 
			}
		}
		return builder.append("  ").toString();
	}
	
	public boolean canGoNorth(final int position) {
		return (this.maze[position] & Directions.NORTH.getDirection()) == 0;
	}
	
	public boolean canGoWest(final int position) {
		return (this.maze[position] & Directions.WEST.getDirection()) == 0;
	}
	
	public boolean canGoSouth(final int position) {
		if (position + width >= size) return false;
		return canGoNorth(position + width);
	}
	
	public boolean canGoEast(final int position) {
		if (position + 1 >= size) return false;
		return canGoWest(position + 1);
	}
}
