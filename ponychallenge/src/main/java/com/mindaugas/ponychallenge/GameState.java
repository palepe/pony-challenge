package com.mindaugas.ponychallenge;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author mindaugas
 *
 */
public class GameState {
	State state;
	@SerializedName("state-result") String stateResult;
}
