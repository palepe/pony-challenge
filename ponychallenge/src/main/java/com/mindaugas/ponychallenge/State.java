package com.mindaugas.ponychallenge;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author mindaugas
 *
 */
public enum State {
	@SerializedName(value="active", alternate={"Active"})
	ACTIVE("active"),
	@SerializedName(value="won", alternate={"Won"})
	WON("won"),
	@SerializedName(value="over", alternate={"Over"})
	OVER("over");
	
	private final String value;
    public String getValue() {
        return value;
    }

    private State(String value) {
        this.value = value;
    }
}
